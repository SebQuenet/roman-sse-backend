const bodyParser = require("body-parser");
const cors = require("cors");

const initMiddlewares = ({ app }) => {
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
};

module.exports = { initMiddlewares };
