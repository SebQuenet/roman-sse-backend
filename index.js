const express = require("express");
const { initMiddlewares } = require("./initMiddlewares");
const { initRoutes } = require("./routes");

const PORT = 3001;

const main = async () => {
  const app = express();

  initMiddlewares({ app });
  initRoutes({ app });

  app.listen(PORT, () => {
    console.log(`Roman listening at http://localhost:${PORT}`);
  });
};

main();
