const { v4: uuidv4 } = require("uuid");

let clients = {};

const sseHeaders = {
  "Content-Type": "text/event-stream",
  Connection: "keep-alive",
  "Cache-Control": "no-cache",
};

const eventsHandler = (req, res) => {
  res.writeHead(200, sseHeaders);
  const clientId = (req.query && req.query.clientId) || uuidv4();

  console.log(`🤝 ${clientId}`);
  const newClient = {
    id: clientId,
    response: res,
  };

  clients[newClient.id] = newClient;

  req.on("close", () => {
    console.log(`❌ ${clientId}`);
    clients = Object.values(clients).filter((client) => client.id !== clientId);
  });
};

const sendEventToClient = ({ clientId }, newRoman) => {
  if (!clients[clientId]) {
    console.log(`❓ Client ${clientId} not found`);
    throw new Error(`Client ${clientId} not found`);
  }

  console.log(`▶ ${clientId} ${newRoman}`);
  clients[clientId].response.write(`data: ${JSON.stringify(newRoman)}\n\n`);
};

const sendEventsToAll = (newRoman) => {
  Object.values(clients).forEach((client) => {
    console.log(`🛰  ${client.id} ${newRoman}`);
    client.response.write(`data: ${JSON.stringify(newRoman)}\n\n`);
  });
};

module.exports = {
  eventsHandler,
  sendEventsToAll,
  sendEventToClient,
};
