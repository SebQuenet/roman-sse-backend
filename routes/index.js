const { eventsHandler } = require("../sse");

const { numeralToRomanController } = require("./numeralToRomanController");

const initRoutes = ({ app }) => {
  app.get("/events", eventsHandler);
  app.post("/numeral/convertToRoman", numeralToRomanController);
};

module.exports = { initRoutes };
