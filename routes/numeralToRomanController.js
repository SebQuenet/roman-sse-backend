const { sendEventsToAll, sendEventToClient } = require("../sse");
const {
  getRomanFromNumeralService,
} = require("../services/romanNumbers/getRomanFromNumeralService");

const numeralToRomanController = (req, res) => {
  const numeral = parseInt(req.body.numeral, 10);
  const clientId = req.query.clientId;
  console.log(clientId);
  const roman = getRomanFromNumeralService(numeral);
  res.json({ ok: true });
  return sendEventToClient({ clientId }, roman);
};

module.exports = { numeralToRomanController };
