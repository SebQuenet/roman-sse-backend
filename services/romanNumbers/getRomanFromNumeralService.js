const oneToNine = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"];
const tenToNinety = [
  "",
  "X",
  "XX",
  "XXX",
  "XL",
  "L",
  "LX",
  "LXX",
  "LXXX",
  "XC",
];
const oneHundredToNineHundred = [
  "",
  "C",
  "CC",
  "CCC",
  "CD",
  "D",
  "DC",
  "DCC",
  "DCCC",
  "CM",
];
const oneToThreeThousands = ["", "M", "MM", "MMM", "", "", "", "", "", ""];

const getRomanFromNumeralService = (numeral) => {
  try {
    const thousands = Math.floor(numeral / 1000);
    const numeralHundreds = numeral - thousands * 1000;

    const hundreds = Math.floor(numeralHundreds / 100);
    const numeralTens = numeralHundreds - hundreds * 100;

    const tens = Math.floor(numeralTens / 10);

    const units = numeral % 10;

    const romanTranscription =
      oneToThreeThousands[thousands] +
      oneHundredToNineHundred[hundreds] +
      tenToNinety[tens] +
      oneToNine[units];

    return romanTranscription;
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

module.exports = { getRomanFromNumeralService };
